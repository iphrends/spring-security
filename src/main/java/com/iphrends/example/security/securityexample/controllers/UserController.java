package com.iphrends.example.security.securityexample.controllers;

import com.iphrends.example.security.securityexample.entities.User;
import com.iphrends.example.security.securityexample.models.BaseResponse;
import com.iphrends.example.security.securityexample.services.impl.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<User>> findById(@PathVariable("id") final Long userId) throws Throwable {
        User user = userService.findById(userId);
        return ResponseEntity.ok(new BaseResponse<>("success", user));
    }
}
