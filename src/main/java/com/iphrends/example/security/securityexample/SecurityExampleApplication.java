package com.iphrends.example.security.securityexample;

import com.iphrends.example.security.securityexample.entities.Authority;
import com.iphrends.example.security.securityexample.entities.User;
import com.iphrends.example.security.securityexample.repositories.AuthorityRepository;
import com.iphrends.example.security.securityexample.services.impl.AuthorityService;
import com.iphrends.example.security.securityexample.services.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.iphrends.example.security.securityexample.repositories")
@EnableTransactionManagement
public class SecurityExampleApplication {

	@Autowired
	private UserService userService;

	@Autowired
	private AuthorityService authorityService;

	public static void main(String[] args) {
		SpringApplication.run(SecurityExampleApplication.class, args);
	}

	@PostConstruct
	public void init() {

		Authority read = new Authority("ROLE_READ");
		authorityService.save(read);
		Authority write = new Authority("ROLE_WRITE");
		authorityService.save(write);

		List<User> users = Arrays.asList(
				new User(1L, "iphrends", "iphrends7@gmail.com", "pwd1"),
				new User(2L, "kashif", "kashif49ful@gmail.com", "pwd2"),
				new User(3L, "kashif38", "kashif38@live.com", "pwd3"),
				new User(4L, "neha", "nehanigar277@gmail.com", "pwd4"),
				new User(5L, "iphrends.tech", "iphrends.tech@gmail.com", "pwd5")
		);
		List<User> userList = users
				.stream()
				.peek(user -> user.setAuthorities(Collections.singletonList(authorityService.findByAuthority("ROLE_READ")))).collect(Collectors.toList());
/*

		List<User> userList = users
				.stream()
				.peek(user -> user.setAuthorities(Collections.singletonList(authorityService.findByAuthority("READ")))).collect(Collectors.toList());
*/

		userService.saveAll(userList);

	}

}
