package com.iphrends.example.security.securityexample.controllers;

import com.iphrends.example.security.securityexample.models.BaseResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @GetMapping
    public ResponseEntity<BaseResponse<Void>> home() {
        return ResponseEntity.ok(new BaseResponse<>("welcome home", null));
    }
}
