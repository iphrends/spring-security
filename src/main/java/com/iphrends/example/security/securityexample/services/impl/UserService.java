package com.iphrends.example.security.securityexample.services.impl;

import com.iphrends.example.security.securityexample.entities.Authority;
import com.iphrends.example.security.securityexample.entities.User;
import com.iphrends.example.security.securityexample.repositories.UserRepository;
import org.springframework.context.annotation.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (null != user) {
            List<Authority> authorities = user.getAuthorities();
            List<GrantedAuthority> grantedAuthorities = authorities.stream().map((Function<Authority, GrantedAuthority>) authority -> new SimpleGrantedAuthority(authority.getAuthority())).collect(Collectors.toList());
            return new org.springframework.security.core.userdetails.User(username, user.getPassword(), grantedAuthorities);
        }
        throw new UsernameNotFoundException("username not available");
    }

    @Secured(value = "ROLE_READ")
//    @PreAuthorize(value = "hasAuthority('ROLE_WRITE')")
    public User findById(Long userId) throws Throwable {
        return userRepository.findById(userId)
                .orElseThrow((Supplier<Throwable>) () -> new UsernameNotFoundException("userId not available"));
    }

    public Page<User> findAll(int page, int limit) {
        return userRepository.findAll(PageRequest.of(page, limit));
    }

    public void saveAll(List<User> users) {
        userRepository.saveAll(users);
    }
}
