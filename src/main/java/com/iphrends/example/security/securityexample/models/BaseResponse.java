package com.iphrends.example.security.securityexample.models;

public class BaseResponse<T> {
    private String message;
    private T result;

    public BaseResponse(String message, T result) {
        this.message = message;
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public T getResult() {
        return result;
    }
}
