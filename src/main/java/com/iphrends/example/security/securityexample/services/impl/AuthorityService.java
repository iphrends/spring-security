package com.iphrends.example.security.securityexample.services.impl;

import com.iphrends.example.security.securityexample.entities.Authority;
import com.iphrends.example.security.securityexample.repositories.AuthorityRepository;
import org.springframework.stereotype.Service;

@Service
public class AuthorityService {

    private final AuthorityRepository authorityRepository;

    public AuthorityService(AuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }

    public Authority save(Authority authority) {
        return authorityRepository.save(authority);
    }

    public Authority findByAuthority(String authority) {
        return authorityRepository.findByAuthority(authority);
    }
}
